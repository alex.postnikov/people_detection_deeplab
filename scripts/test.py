#!/usr/bin/python
import concurrent.futures
from multiprocessing import Pool
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image, PointCloud2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Point32
import cv2
import numpy as np
import time
import tf
bridge = CvBridge()

TRESHOLD = .50

p = Point32()
p.x = 0;    p.y = 1;    p.z = 0
point_p = np.array((p.x,p.y,p.z))
point_p1 = np.array((p.x+1,p.y+2,p.z+3))
point_p2 = np.array((p.x+2,p.y+2,p.z+3))
point_p3 = np.array((p.x+4,p.y+2,p.z+3))
points = [point_p,point_p1,point_p2,point_p3]



def find_indexes(point):
    dist = []
    # point = listener.transformPoint("base", point)
    for ind , pp in enumerate(points):
            point_pcl = np.array((point.x, point.y, point.z))
            d = find_dist(point_pcl, pp)
            if d != None:
                dist.append(ind, d)
    if dist != []:
        return dist

def find_dist(point_pcl, pp):
    
    dist = abs(np.linalg.norm(point_pcl - pp))
    if dist < TRESHOLD:
        return dist#, pp)
    else:
        return None

def callback(data):
    global pub, pubP

    bridge = CvBridge()

    d_image = bridge.imgmsg_to_cv2(data, "16UC1")

    d_image = cv2.blur(d_image,(5,5))
    
    t = np.uint8(d_image/255.0)
    edges = cv2.Canny(t ,5, 7)
    
    cv2.imshow("depth", d_image)
    cv2.imshow("edges", edges)
    cv2.waitKey(1)

    # sensor_msgs/Image

def callback_pcl_single(msg):
    dist = []
    start = time.time()
    msg = listener.transformPointCloud("base", msg)
    for point in msg.points:
        for ind, pp in enumerate(points):
                point_pcl = np.array((point.x, point.y, point.z))
                d = find_dist(point_pcl, pp)
                if d != None:
                    dist.append([ind, d])
    
    rospy.loginfo(time.time() - start)
    dist_ind = [t[0] for t in dist]
    dist_ind_set = set(dist_ind)
    for i in dist_ind_set:
        if dist_ind.count(i) > 10 :
            print (dist_ind.count(i))
            print (i)
    # print dist

def callback_pcl_parallel(msg):
        
    dist = []
    sub_list_len = 100
    sub_lists = ([msg.points[i:i + sub_list_len] for i in xrange(0, len(msg.points), sub_list_len)])
    print len(sub_lists)
    print len(sub_lists[0])
    start = time.time()
    for sub_list in sub_lists:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            for out1 in executor.map(find_indexes, sub_list):
                if out1 != []: 
                    dist.append(out1)
    rospy.loginfo(time.time() - start)

def callback_pcl_multip(msg):
    
    start = time.time()
    msg = listener.transformPointCloud("base", msg)
    res = p.map(find_indexes, msg.points)
    
    rospy.loginfo(time.time() - start)
    # res = [x for x in res if x is not None]
    # print res


def l():
    
    global pub, pubP, listener
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)
    listener = tf.TransformListener()

    # rospy.Subscriber("/camera/aligned_depth_to_color/image_raw", Image, callback)
    
    rospy.Subscriber("persons_pcl", PointCloud, callback_pcl_single)
    # rospy.Subscriber("persons_pcl", PointCloud, callback_pcl_parallel)
    # rospy.Subscriber("persons_pcl", PointCloud, callback_pcl_multip)
    
    
    # pub = rospy.Publisher('chatter', Image, queue_size=10)
    # pubP = rospy.Publisher('chatterP', PointCloud2, queue_size=10)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    l()
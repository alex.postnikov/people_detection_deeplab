import rospy
from geometry_msgs.msg import PointStamped
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Polygon, Point32, Point

class MarkerFromXYZ():

    def __init__(self, p0,p1,z,c):
        self.state = PointStamped()

        self.robotMarker = Marker()
        self.robotMarker.header.frame_id = "world"
        self.robotMarker.header.stamp    = rospy.get_rostime()

        self.robotMarker.id = c
        self.robotMarker.type = 0 # sphere
        self.robotMarker.action = 0
        p = Point()
        p.x = p0[0]
        p.y = p0[1]
        p.z = z[0]
        self.robotMarker.points.append(p)
        p_ = Point()
        p_.x = p1[0]
        p_.y = p1[1]
        p_.z = z[1]
        self.robotMarker.points.append(p_)
        # self.robotMarker.pose.position = self.state.point
        # self.robotMarker.pose.orientation.x = 0
        # self.robotMarker.pose.orientation.y = 0
        # self.robotMarker.pose.orientation.z = 0
        # self.robotMarker.pose.orientation.w = 1.0
        self.robotMarker.scale.x = 0.05
        self.robotMarker.scale.y = 0.05
        self.robotMarker.scale.z = 0.05
        
        self.robotMarker.color.r = 0.0
        self.robotMarker.color.g = 1.0
        self.robotMarker.color.b = 0.0
        self.robotMarker.color.a = 0.5

        self.robotMarker.lifetime = rospy.Duration(0.5)
    
    def get_marker(self):
        return self.robotMarker



class CylinderFromXYZ():

    def __init__(self, p0,p1,z,c, width = 1):
        self.state = PointStamped()

        self.marker = Marker()
        self.marker.header.frame_id = "world"
        self.marker.header.stamp    = rospy.get_rostime()

        self.marker.id = c
        self.marker.type = 1 # cube
        self.marker.action = 0
        
        self.marker.pose.position.x = (p0[0] + p1[0])/2.0
        self.marker.pose.position.y = (p0[1] + p1[1])/2.0
        self.marker.pose.position.z = z[0]
        self.marker.pose.orientation.x = 0
        self.marker.pose.orientation.y = 0
        self.marker.pose.orientation.z = 0
        self.marker.pose.orientation.w = 1.0
        self.marker.scale.x = width
        self.marker.scale.y = abs(z[1] - z[0])/0.9
        self.marker.scale.z = width
        self.marker.color.r = 0.0
        self.marker.color.g = 1.0
        self.marker.color.b = 0.0
        self.marker.color.a = .1

        self.marker.lifetime = rospy.Duration(0.5)
    
    def get_marker(self):
        return self.marker


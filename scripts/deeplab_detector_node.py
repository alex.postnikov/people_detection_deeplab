#!/usr/bin/python
import rospy
import rospkg

from cv_bridge import CvBridge, CvBridgeError
import cv2
import tf
import sensor_msgs.msg
from sensor_msgs.msg import CameraInfo, PointCloud2, PointCloud, ChannelFloat32
from geometry_msgs.msg import Polygon, Point32, Point, PointStamped

from visualization_msgs.msg import MarkerArray, Marker
from message_filters import ApproximateTimeSynchronizer, Subscriber
from marker_publishing import MarkerFromXYZ, CylinderFromXYZ

import numpy as np
import time
from PIL import Image
from deeplab_model import MODEL, label_to_color_image
import struct
import math


# DEBUG = 0

# RESIZED_IMG_SIZE = [240, 0]
# if DEBUG:
#     RESIZED_IMG_SIZE[0] *= 2
# RESIZED_IMG_SIZE[1] = int(RESIZED_IMG_SIZE[0]*0.75)


def remap_pixel(x, y, orig_size, new_size):
    x = (x - new_size[0]/2.0) * new_size[0]/new_size[0] + orig_size[0]/2.0
    y = (y - new_size[1]/2.0) * new_size[1]/new_size[1] + orig_size[1]/2.0
    return int(x), int(y)

class detector_node():

    def __init__(self):
        
        rospy.logwarn("waiting for CameraInfo")
        self.params = self.init_params()
        self.camInfo = rospy.wait_for_message('/camera/aligned_depth_to_color/camera_info', CameraInfo)
        self.process_caminfo()
        rospy.logwarn("CameraInfo received")

        self.bridge = CvBridge()
        self.d_image = None
        # self.d_data = None
        self.update_d_image_flag = True

        rospy.logwarn("waiting for transofrm from world to camera")
        self.t = tf.TransformListener()
        try:
            self.t.waitForTransform("kinect2_ir_optical_frame", "world", rospy.Time.now(), rospy.Duration(4.0))
            self.trans, self.rot = self.t.lookupTransform("kinect2_ir_optical_frame", "world", rospy.Duration(4.0))
            quaternion = (
                self.rot[0],
                self.rot[1],
                self.rot[2],
                self.rot[3],)
            self.euler = tf.transformations.euler_from_quaternion(quaternion)
            
        except:
            rospy.logwarn("cannot receive transofrm from world to camera")

        self.image_sub = Subscriber("/camera/color/image_raw",sensor_msgs.msg.Image)
        self.image_d_sub = Subscriber("/camera/aligned_depth_to_color/image_raw",sensor_msgs.msg.Image)
        self.pointcloud_pub = rospy.Publisher("persons_pcl", PointCloud, queue_size=1)
        self.ts_counter = [0,-1]
        self.ts = ApproximateTimeSynchronizer([self.image_sub, self.image_d_sub], queue_size = 10, slop = 0.2)
        self.ts.registerCallback(self.rgbd_image_callback)
        self.marker_pub = rospy.Publisher("person_markers", MarkerArray, queue_size=1)
        rospy.Timer(rospy.Duration(2), self.clock_cb)
        self.clustered_im_pub = rospy.Publisher("/camera/clustered", sensor_msgs.msg.Image, queue_size=1)


    def init_params(self):
        parameters = {}
        parameters["debug"] = rospy.get_param("debug", False)
        parameters["do_canny"] = rospy.get_param("do_canny", False)
        parameters["resized_img_w"] = rospy.get_param("resized_img_w", 240)
        parameters["resized_img_h"] = rospy.get_param("resized_img_h", 180)
        parameters["resized_img_size"] = [parameters["resized_img_w"], parameters["resized_img_h"]]
        parameters["image_cb_delay_max_ms"] = rospy.get_param("image_cb_delay_max_ms", 100)
        
        return parameters

    def clock_cb(self, event):
        
        if self.ts_counter[1] == self.ts_counter[0]:
            now = rospy.get_rostime()
            rospy.logwarn("it stopped?")
            # self.ts = ApproximateTimeSynchronizer([self.image_sub, self.image_d_sub], queue_size = 10, slop = 0.2)
            # self.ts.registerCallback(self.rgbd_image_callback)

        self.ts_counter[1] = self.ts_counter[0]

    def process_caminfo(self):
        self.m_fx = self.camInfo.K[0] * self.params["resized_img_size"][0]/640.0 
        self.m_fy = self.camInfo.K[4] * self.params["resized_img_size"][1]/480.0 
        self.m_cx = self.camInfo.K[2] * self.params["resized_img_size"][0]/640.0 
        self.m_cy = self.camInfo.K[5] * self.params["resized_img_size"][1]/480.0 
        self.inv_fx = 1 / self.m_fx
        self.inv_fy = 1 / self.m_fy

    def from_depth_map_to_3d(self, u, v, d):

        point = Point32()
        point.z = d * 0.001
        if self.params["debug"]:
            if point.z > 5:
                return None
        point.x = (u - self.m_cx) * point.z * self.inv_fx
        point.y = 1 * ((v - self.m_cy) * point.z * self.inv_fy)
        return point

    def rgbd_image_callback(self, rgb_data, d_data):
        
        # left only "new" images
        self.ts_counter[0] += 1 
        now = rospy.get_rostime()
        delta_ms = (now.secs*(10**9) + now.nsecs - rgb_data.header.stamp.secs*(10**9) - rgb_data.header.stamp.nsecs)/ 1000000.0
        if delta_ms > self.params["image_cb_delay_max_ms"]:
            return

        # receive imgs
        self.d_image = self.bridge.imgmsg_to_cv2(d_data, "16UC1")
        cv_image = self.bridge.imgmsg_to_cv2(rgb_data, "bgr8")
        cv_image = cv2.cvtColor(cv_image,cv2.COLOR_BGR2RGB)
        pil_im = Image.fromarray(cv_image)

        # do DNN magic
        resized_im, seg_map = MODEL.run(pil_im)

        #  prepare cvimage from DNN output
        self.seg_image = np.array(label_to_color_image(seg_map).astype(np.uint8))[:, :, ::-1].copy()         
        self.resized_im_ = np.array(resized_im)[:, :, ::-1].copy()         
        
        self.seg_image = cv2.resize(self.seg_image, (self.params["resized_img_size"][0], self.params["resized_img_size"][1]))
        self.d_image = cv2.resize(self.d_image, (self.params["resized_img_size"][0], self.params["resized_img_size"][1]))
        self.resized_im_ =cv2.resize(self.resized_im_, (self.params["resized_img_size"][0], self.params["resized_img_size"][1]))
        if self.params["do_canny"] == True:
            d_image_processed = cv2.blur(self.d_image,(5,5))
            d_image_processed = np.uint8(d_image_processed/255.0)
            edges = cv2.Canny(d_image_processed ,6, 7)
            kernel = np.ones((3,3),np.uint8)
            edges = cv2.dilate(edges,kernel,iterations = 1)
            self.seg_image[:,:,0] *= ((255-edges)/255)
            self.seg_image[:,:,1] *= ((255-edges)/255)
            self.seg_image[:,:,2] *= ((255-edges)/255)
        # self.create_pointcl()
        
        

        try:
            self.create_markers()
        except Exception as e:
            raise (e)

        self.clustered_im_pub.publish(self.bridge.cv2_to_imgmsg(self.seg_image, "bgr8"))
        # create pointcloud from segmented image and depth image


    def create_markers(self):
        
        marker_pos = self.get_tob_bot_depth_contours_points_from_seg_depth(self.seg_image)
        mark_array = MarkerArray()
        
        marker_counter = 0
        if len(marker_pos) > 0:
            for p0,p1,z in marker_pos:

                start  = self.from_depth_map_to_3d(p0[0], p0[1], z)
                start_pointStamped = PointStamped()
                start_pointStamped.header.frame_id = "kinect2_ir_optical_frame"
                start_pointStamped.point = start
                start_pointStamped_world = self.t.transformPoint("world", start_pointStamped)
                stop  = self.from_depth_map_to_3d(p1[0], p1[1], z)
                stop_pointStamped = PointStamped()
                stop_pointStamped.header.frame_id = "kinect2_ir_optical_frame"
                stop_pointStamped.point = stop
                stop_pointStamped_world = self.t.transformPoint("world", stop_pointStamped)

                try:
                    # m = MarkerFromXYZ( (start_pointStamped_world.point.x, start_pointStamped_world.point.y),(stop_pointStamped_world.point.x , stop_pointStamped_world.point.y) ,(start_pointStamped_world.point.z, stop_pointStamped_world.point.z), marker_counter)
                    m = MarkerFromXYZ( (start_pointStamped_world.point.x, start_pointStamped_world.point.y),(start_pointStamped_world.point.x , start_pointStamped_world.point.y) ,(start_pointStamped_world.point.z, start_pointStamped_world.point.z-1), marker_counter)
                    mark_array.markers.append(m.get_marker())
                    marker_counter += 1
                    c = CylinderFromXYZ( (start_pointStamped_world.point.x, start_pointStamped_world.point.y),(start_pointStamped_world.point.x , start_pointStamped_world.point.y) ,(start_pointStamped_world.point.z, start_pointStamped_world.point.z-1), marker_counter)
                    mark_array.markers.append(c.get_marker())
                    marker_counter += 1
                except Exception as e:
                    raise (e)
        self.marker_pub.publish(mark_array)

    def create_pointcl(self):

        # resize images for speed-up calculus
        

        # for each pixel colored with "person color" create 3d point and push to pointcloud
        height, width, ch = self.seg_image.shape
        pointcl = PointCloud()
        pointcl.header.frame_id = "kinect2_ir_optical_frame"
        pointcl.header.stamp = rospy.get_rostime()
        rgb = ChannelFloat32()
        rgb.name = "rgb"

        for v in range(width):
            for u in range(height):
                # "person cluster" 128,128,xxx
                if (self.seg_image[u][v][0] == 128) and (self.seg_image[u][v][1] == 128):
                    # rospy.logerr(self.seg_image[u][v][2])
                    # convert from u,v,depth to 3d
                    p = self.from_depth_map_to_3d(v,u,self.d_image[u][v])
                    if p != None:
                        pointcl.points.append(p)
                        r = self.resized_im_[u][v][0]
                        g = self.resized_im_[u][v][1]
                        b = self.resized_im_[u][v][2]
                        rgb_packed = struct.pack('<BBBB',r,g,b,0) #rgb #r,b,g #b,g,r
                        rgb_unpacked = float(struct.unpack("<f", rgb_packed)[0])
                        rgb.values.append(rgb_unpacked)
                else:
                    if self.params["debug"]:
                        p = self.from_depth_map_to_3d(v,u,self.d_image[u][v])
                        if p != None:
                            pointcl.points.append(p)
                            r = self.resized_im_[u][v][0]
                            g = self.resized_im_[u][v][1]
                            b = self.resized_im_[u][v][2]
                            rgb_packed = struct.pack('<BBBB',r,g,b,0) #rgb #r,b,g #b,g,r

                            rgb_unpacked = float(struct.unpack("<f", rgb_packed)[0])
                            rgb.values.append(rgb_unpacked)

                    else:
                        self.seg_image[u][v] = [0,0,0]
        pointcl.channels.append(rgb)
        self.pointcloud_pub.publish(pointcl)




    def get_tob_bot_depth_contours_points_from_seg_depth(self, img):
        
        marker_pos = []

        # find contours
        imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(imgray,127,255,0)
        im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # for each contour find "mean" depth
        for ctr in contours:
            
            area = cv2.contourArea(ctr)
            if (area < 500):
                continue
            M = cv2.moments(ctr)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            d = self.d_image[cy - 3 : cy + 4, cx - 3 : cx + 4]
            d = np.mean(d)
            x, y, w, h = cv2.boundingRect(ctr)

            # cv2.circle(im2, (x+ w/2, y+1), 5, 100, -1)
            # cv2.circle(im2, (x+ w/2, y+h-1), 5, 100, -1)
            x_i = x + w/2 
            y_i = y + h/2
            if (imgray[cy][cx]) == 147:
                marker_pos.append([(x+ w/2, y+1), (x+ w/2, y+h-1), d])

        
        return marker_pos

if __name__ == '__main__':

    rospy.init_node('deeplab_detector', anonymous=True)
    node = detector_node()
    rospy.spin()
